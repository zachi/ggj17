﻿using System;
using AnimPlugin;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AnimPlugin.Collections
{
	public enum EnumerationMode
	{
		AtOnce,
		Chain,
		Parallel
	}

	public static class Col
	{
		#region Fields

		public static EnumerationMode EnumerationModeDefault = EnumerationMode.Chain;

		#endregion

		#region Iterate action over all elements of collection

		/**
		 * Iterates over values,
		 * Returns true if all of them are true
		 */
		public static bool IfEach(params bool[] values) {
			for (int i = 0; i < values.Length; i++) {
				if (!values [i])
					return false;
			}
			return true;
		}

		/**
		 * Iterate over values,
		 * Returns true if any of them are true
		 */
		public static bool IfAny(params bool[] values) {
			for (int i = 0; i < values.Length; i++) {
				if (values [i])
					return true;
			}
			return false;
		}

		public static IEnumerator Each<V> (IList<V> collection, Action<V> action)
		{
			return Each<V> (collection, action, EnumerationModeDefault);
		}

		public static IEnumerator Each<V> (IList<V> collection, Action<V> action, EnumerationMode mode)
		{
			if (mode == EnumerationMode.AtOnce) {
				for (int i = 0; i < collection.Count; i++) {
					action (collection[i]);
				}
				yield return null;
			} else if (mode == EnumerationMode.Chain) {
				yield return A.Chain (ActionCollections<V> (collection, action));
			} else if (mode == EnumerationMode.Parallel) {
				yield return A.Parallel (ActionCollections<V> (collection, action));
			}
		}

		public static IEnumerator ForEachOf<V> (IList<V> collection, Action<V, int> action)
		{
			return ForEachOf<V> (collection, action, EnumerationModeDefault);
		}

		public static IEnumerator ForEachOf<V> (IList<V> collection, Action<V, int> action, EnumerationMode mode)
		{
			if (mode == EnumerationMode.AtOnce) {
				for (int i = 0; i < collection.Count; i++) {
					action (collection [i], i);
				}
				yield return null;
			} else if (mode == EnumerationMode.Chain) {
				yield return A.Chain (ActionCollections<V> (collection, action));
			} else if (mode == EnumerationMode.Parallel) {
				yield return A.Parallel (ActionCollections<V> (collection, action));
			}
		}

		#endregion

		#region Mapping collection throught iterator

		public static ResultType[] Map<SourceType, ResultType> (IList<SourceType> collection, Func<SourceType, ResultType> func)
		{
			ResultType[] result = new ResultType[collection.Count];
			for (int i = 0; i < collection.Count; i++) {
				result [i] = func (collection [i]);
			}
			return result;
		}

		public static Dictionary<K, V> CreateDict<K, V> (IList<V> collection, Func<V, K> createKeyFunction)
		{
			Dictionary<K, V> result = new Dictionary<K, V> ();
			for (int i = 0; i < collection.Count; i++) {
				result.Add (createKeyFunction (collection [i]), collection [i]);
			}
			return result;
		}

		#endregion

		#region TOOLS - mapping action's collection to IEnumerator[]

		private static IEnumerator[] ActionCollections<V> (IList<V> collection, Action<V> action)
		{
			IEnumerator[] anims = new IEnumerator[collection.Count];
			for (int i = 0; i < collection.Count; i++) {
				anims [i] = A.Void (action, collection [i]);
			}
			return anims;
		}

		private static IEnumerator[] ActionCollections<V> (IList<V> collection, Action<V, int> action)
		{
			IEnumerator[] anims = new IEnumerator[collection.Count];
			for (int i = 0; i < collection.Count; i++) {
				anims [i] = A.Void (action, collection [i], i);
			}
			return anims;
		}

		#endregion
	}
}

