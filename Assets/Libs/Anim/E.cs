﻿/**
 * This file is a part of Unity AnimPlugin
 *
 * Animates the float value between two target values using 
 * Robert Penner's easing equations for interpolation over a specified Duration.
 *
 * For use in Unity - System.Math were replaced by UnityEngine.Mathf
 * and typeof double were replaced with floats.
 *
 * @author      Szymon 'Zachi' Zachara (szymon.zachara@gmail.com)
 *
 * Credit/Thanks:
 * Robert Penner - The easing equations we all know and love 
 *   (http://robertpenner.com/easing/) [See License.txt for license info]
 * 
 * Lee Brimelow - initial port of Penner's equations to WPF 
 *   (http://thewpfblog.com/?p=12)
 * 
 * Zeh Fernando - additional equations (out/in) from 
 *   caurina.transitions.Tweener (http://code.google.com/p/tweener/)
 *   [See License.txt for license info]
 * 
 * Darren David - Porting easing equations to C#
 *   (http://wpf-animation.googlecode.com/)
 *   [See Licence.txt for licence info]
 *
 */
using UnityEngine;

namespace AnimPlugin
{
	public static class E
	{
		#region Easings implementation for Unity3d

		#region Linear

		public static float Linear (float t, float b, float c, float d)
		{
			return c * t / d + b;
		}

		#endregion

		#region Expo

		public static float ExpoIn (float t, float b, float c, float d)
		{
			return (t == 0) ? b : c * Mathf.Pow (2, 10 * (t / d - 1)) + b;
		}

		public static float ExpoOut (float t, float b, float c, float d)
		{
			return (t == d) ? b + c : c * (-Mathf.Pow (2, -10 * t / d) + 1) + b;
		}

		public static float ExpoInOut (float t, float b, float c, float d)
		{
			if (t == 0)
				return b;
			if (t == d)
				return b + c;
			if ((t /= d / 2) < 1)
				return c / 2 * Mathf.Pow (2, 10 * (t - 1)) + b;
			return c / 2 * (-Mathf.Pow (2, -10 * --t) + 2) + b;
		}

		public static float ExpoOutIn (float t, float b, float c, float d)
		{
			if (t < d / 2)
				return ExpoOut (t * 2, b, c / 2, d);

			return ExpoIn ((t * 2) - d, b + c / 2, c / 2, d);
		}

		#endregion

		#region Circural

		public static float CircIn (float t, float b, float c, float d)
		{
			return -c * (Mathf.Sqrt (1 - (t /= d) * t) - 1) + b;
		}

		public static float CircOut (float t, float b, float c, float d)
		{
			return c * Mathf.Sqrt (1 - (t = t / d - 1) * t) + b;   
		}

		public static float CircInOut (float t, float b, float c, float d)
		{
			if ((t /= d / 2) < 1)
				return -c / 2 * (Mathf.Sqrt (1 - t * t) - 1) + b;
			return c / 2 * (Mathf.Sqrt (1 - (t -= 2) * t) + 1) + b;
		}

		public static float CircOutIn (float t, float b, float c, float d)
		{
			if (t < d / 2)
				return CircOut (t * 2, b, c / 2, d);

			return CircIn ((t * 2) - d, b + c / 2, c / 2, d);
		}

		#endregion

		#region Quad

		public static float QuadIn (float t, float b, float c, float d)
		{
			return c * (t /= d) * t + b;
		}

		public static float QuadOut (float t, float b, float c, float d)
		{
			return -c * (t /= d) * (t - 2) + b;
		}

		public static float QuadInOut (float t, float b, float c, float d)
		{
			if ((t /= d / 2) < 1)
				return c / 2 * t * t + b;
			return -c / 2 * ((--t) * (t - 2) - 1) + b;
		}

		public static float QuadOutIn (float t, float b, float c, float d)
		{
			if (t < d / 2)
				return QuadOut (t * 2, b, c / 2, d);
			return QuadIn ((t * 2) - d, b + c / 2, c / 2, d);
		}

		#endregion

		#region Sine

		public static float SineIn (float t, float b, float c, float d)
		{
			return -c * Mathf.Cos (t / d * (Mathf.PI / 2)) + c + b;
		}

		public static float SineOut (float t, float b, float c, float d)
		{
			return c * Mathf.Sin (t / d * (Mathf.PI / 2)) + b;
		}

		public static float SineInOut (float t, float b, float c, float d)
		{
			return -c / 2 * (Mathf.Cos (Mathf.PI * t / d) - 1) + b;
		}

		public static float SineOutIn (float t, float b, float c, float d)
		{
			if ((t /= d / 2) < 1)
				return c / 2 * (Mathf.Sin (Mathf.PI * t / 2)) + b;
			return -c / 2 * (Mathf.Cos (Mathf.PI * --t / 2) - 2) + b;
		}

		#endregion

		#region Cubic

		public static float CubicIn (float t, float b, float c, float d)
		{
			return c * (t /= d) * t * t + b;
		}

		public static float CubicOut (float t, float b, float c, float d)
		{
			return c * ((t = t / d - 1) * t * t + 1) + b;
		}

		public static float CubicInOut (float t, float b, float c, float d)
		{
			if ((t /= d / 2) < 1)
				return c / 2 * t * t * t + b;
			return c / 2 * ((t -= 2) * t * t + 2) + b;
		}

		public static float CubicOutIn (float t, float b, float c, float d)
		{
			if (t < d / 2)
				return CubicOut (t * 2, b, c / 2, d);

			return CubicIn ((t * 2) - d, b + c / 2, c / 2, d);
		}

		#endregion

		#region Quartic

		public static float QuartIn (float t, float b, float c, float d)
		{
			return c * (t /= d) * t * t * t + b;
		}

		public static float QuartOut (float t, float b, float c, float d)
		{
			return -c * ((t = t / d - 1) * t * t * t - 1) + b;
		}

		public static float QuartInOut (float t, float b, float c, float d)
		{
			if ((t /= d / 2) < 1)
				return c / 2 * t * t * t * t + b;
			return -c / 2 * ((t -= 2) * t * t * t - 2) + b;
		}

		public static float QuartOutIn (float t, float b, float c, float d)
		{
			if (t < d / 2)
				return QuartOut (t * 2, b, c / 2, d);
			return QuartIn ((t * 2) - d, b + c / 2, c / 2, d);
		}

		#endregion

		#region Quintic

		public static float QuintIn (float t, float b, float c, float d)
		{
			return c * (t /= d) * t * t * t * t + b;
		}

		public static float QuintOut (float t, float b, float c, float d)
		{
			return c * ((t = t / d - 1) * t * t * t * t + 1) + b;
		}

		public static float QuintInOut (float t, float b, float c, float d)
		{
			if ((t /= d / 2) < 1)
				return c / 2 * t * t * t * t * t + b;
			return c / 2 * ((t -= 2) * t * t * t * t + 2) + b;
		}

		public static float QuintOutIn (float t, float b, float c, float d)
		{
			if (t < d / 2)
				return QuintOut (t * 2, b, c / 2, d);
			return QuintIn ((t * 2) - d, b + c / 2, c / 2, d);
		}

		#endregion

		#region Elastic

		public static float ElasticIn (float t, float b, float c, float d)
		{
			if ((t /= d) == 1f)
				return b + c;

			float p = d * .3f;
			float s = p / 4f;

			return -(c * Mathf.Pow (2f, 10f * (t -= 1f)) * Mathf.Sin ((t * d - s) * (2f * Mathf.PI) / p)) + b;
		}

		public static float ElasticOut (float t, float b, float c, float d)
		{
			if ((t /= d) == 1f)
				return b + c;

			float p = d * .3f;
			float s = p / 4f;

			return (c * Mathf.Pow (2f, -10f * t) * Mathf.Sin ((t * d - s) * (2f * Mathf.PI) / p) + c + b);
		}

		public static float ElasticInOut (float t, float b, float c, float d)
		{
			if ((t /= d / 2f) == 2f)
				return b + c;

			float p = d * (.3f * 1.5f);
			float s = p / 4f;

			if (t < 1f)
				return -.5f * (c * Mathf.Pow (2f, 10f * (t -= 1f)) * Mathf.Sin ((t * d - s) * (2f * Mathf.PI) / p)) + b;
			return c * Mathf.Pow (2f, -10f * (t -= 1f)) * Mathf.Sin ((t * d - s) * (2f * Mathf.PI) / p) * .5f + c + b;
		}

		public static float ElasticOutIn (float t, float b, float c, float d)
		{
			if (t < d / 2)
				return ElasticOut (t * 2, b, c / 2, d);
			return ElasticIn ((t * 2) - d, b + c / 2, c / 2, d);
		}

		#endregion

		#region Bounce

		public static float BounceIn (float t, float b, float c, float d)
		{
			return c - BounceOut (d - t, 0, c, d) + b;
		}

		public static float BounceOut (float t, float b, float c, float d)
		{
			if ((t /= d) < (1f / 2.75f)) {
				return c * (7.5625f * t * t) + b;
			} else if (t < (2f / 2.75f)) {
				return c * (7.5625f * (t -= (1.5f / 2.75f)) * t + .75f) + b;
			} else if (t < (2.5f / 2.75f)) {
				return c * (7.5625f * (t -= (2.25f / 2.75f)) * t + .9375f) + b;
			} else {
				return c * (7.5625f * (t -= (2.625f / 2.75f)) * t + .984375f) + b;
			}
		}

		public static float BounceInOut (float t, float b, float c, float d)
		{
			if (t < d / 2f)
				return BounceIn (t * 2f, 0f, c, d) * .5f + b;
			return BounceOut (t * 2f - d, 0f, c, d) * .5f + c * .5f + b;
		}

		public static float BounceOutIn (float t, float b, float c, float d)
		{
			if (t < d / 2)
				return BounceOut (t * 2, b, c / 2, d);
			return BounceIn ((t * 2) - d, b + c / 2, c / 2, d);
		}

		#endregion

		#region Back

		public static float BackIn (float t, float b, float c, float d)
		{
			float s = 1.70158f;
			return c * (t /= d) * t * ((s + 1) * t - s) + b;
		}

		public static float BackOut (float t, float b, float c, float d)
		{
			float s = 1.70158f;
			return c * ((t = t / d - 1) * t * ((s + 1) * t + s) + 1) + b;
		}

		public static float BackInOut (float t, float b, float c, float d)
		{
			float s = 1.70158f;
			if ((t /= d / 2f) < 1f)
				return c / 2f * (t * t * (((s *= (1.525f)) + 1f) * t - s)) + b;
			return c / 2f * ((t -= 2f) * t * (((s *= (1.525f)) + 1f) * t + s) + 2f) + b;
		}

		public static float BackOutIn (float t, float b, float c, float d)
		{
			if (t < d / 2)
				return BackOut (t * 2, b, c / 2, d);
			return BackIn ((t * 2) - d, b + c / 2, c / 2, d);
		}

		#endregion

		#endregion
	}
}