﻿/**
 * This file is a part of Unity AnimPlugin
 *
 * @author      Szymon 'Zachi' Zachara (szymon.zachara@gmail.com)
 */

using UnityEngine;
using System.Collections;
using System;

namespace AnimPlugin
{
	#region Helpers: Easing function delegate

	public delegate float EasingFunction (
        float currTime,
        float beginningValue,
        float desiredValue,
        float duration);

	public enum TimeMode {
		NORMAL,
		UNSCALED
	}
	#endregion

	public static class A
	{
		#region static Lerp function replacement

		public static float Lerp (float minimum, float maximum, float time)
		{
			return E.Linear (time, minimum, maximum - minimum, 1f);
		}

		public static float Lerp (float minimum, float maximum, float time, EasingFunction f)
		{
			return f (time, minimum, maximum - minimum, 1f);
		}

		public static Vector2 Lerp (Vector2 minimum, Vector2 maximum, float time)
		{
			return Vector2.Lerp (minimum, maximum, time);
		}

		public static Vector2 Lerp (Vector2 minimum, Vector2 maximum, float time, EasingFunction f)
		{
			return Vector2.LerpUnclamped (minimum, maximum, f (time, 0f, 1f, 1f));
		}

		public static Vector3 Lerp (Vector3 minimum, Vector3 maximum, float time)
		{
			return Vector3.Lerp (minimum, maximum, time);
		}

		public static Vector3 Lerp (Vector3 minimum, Vector3 maximum, float time, EasingFunction f)
		{
			return Vector3.LerpUnclamped (minimum, maximum, f (time, 0f, 1f, 1f));
		}

		public static Vector4 Lerp (Vector4 minimum, Vector4 maximum, float time)
		{
			return Vector4.Lerp (minimum, maximum, time);
		}

		public static Vector4 Lerp (Vector4 minimum, Vector4 maximum, float time, EasingFunction f)
		{
			return Vector4.LerpUnclamped (minimum, maximum, f (time, 0f, 1f, 1f));
		}

		public static Color Lerp (Color minimum, Color maximum, float time)
		{
			return Color.Lerp(minimum, maximum, time);
		}

		public static Color Lerp (Color minimum, Color maximum, float time, EasingFunction f)
		{
			return Color.LerpUnclamped (minimum, maximum, f (time, 0f, 1f, 1f));
		}

		public static Quaternion Lerp(Quaternion minimum, Quaternion maximum, float time) {
			return Quaternion.Lerp (minimum, maximum, time);
		}

		public static Quaternion Lerp(Quaternion minimum, Quaternion maximum, float time, EasingFunction f) {
			return Quaternion.LerpUnclamped (minimum, maximum, f (time, 0f, 1f, 1f));
		}

		public static Quaternion Slerp(Quaternion minimum, Quaternion maximum, float time) {
			return Quaternion.Slerp (minimum, maximum, time);
		}

		public static Quaternion Slerp(Quaternion minimum, Quaternion maximum, float time, EasingFunction f) {
			return Quaternion.SlerpUnclamped (minimum, maximum, f (time, 0f, 1f, 1f));
		}

		#endregion

		#region Performing animation on basic datatypes

		/**
		 * Performs 'function' until 'function' returns 'desiredValue'
		 */
		public static IEnumerator Until<V>(Func<V> function, V desiredValue) where V:IEquatable<V> {
			V returnedVal;
			do {
				returnedVal = function.Invoke();
				yield return null;
			} while (!desiredValue.Equals(returnedVal));
		}

		public static IEnumerator PerformFunction<V> (Func<V, V> function, V initialVal, V desiredVal) where V:IEquatable<V>
		{
			V currVal = initialVal;
			do {
				currVal = function (currVal);
				yield return null;
			} while(!currVal.Equals (desiredVal));
		}

		public static IEnumerator PerformFunction<V> (Func<V, V> function, V val, Func<V, bool> doContinue) where V:class
		{
			while (doContinue (val)) {
				val = function (val);
				yield return null;
			}
		}

		public static IEnumerator PerformFunction (Action<float> action, float initialValue, float desiderValue, float animDuration)
		{
			return PerformFunction (action, initialValue, desiderValue, animDuration, E.Linear);
		}

		public static IEnumerator PerformFunction (Action<float> action, float initialValue, float desiredValue, float animDuration, EasingFunction f)
		{
			float dF = desiredValue - initialValue;
			float sumTime = 0f;
			while (sumTime < animDuration) {
				action (
					f (
						sumTime, 
						initialValue, 
						dF, 
						animDuration
					)
				);
				sumTime += Time.deltaTime;
				yield return null;
			}
			//need to be sure
			action (desiredValue);
		}

		public static IEnumerator PerformFunction (Action<Vector2> action, Vector2 initialValue, Vector2 desiredValue, float animDuration)
		{
			return PerformFunction (action, initialValue, desiredValue, animDuration, E.Linear);
		}

		public static IEnumerator PerformFunction (Action<Vector2> action, Vector2 initialValue, Vector2 desiredValue, float animDuration, EasingFunction f)
		{
			float dX = desiredValue.x - initialValue.x;
			float dY = desiredValue.y - initialValue.y;
			float sumTime = 0f;
			while (sumTime < animDuration) {
				action (
					new Vector3 (
						f (sumTime, initialValue.x, dX, animDuration),
						f (sumTime, initialValue.y, dY, animDuration)
					)
				);
				sumTime += Time.deltaTime;
				yield return null;
			}
			//need to be sure
			action (desiredValue);   
		}

		public static IEnumerator PerformFunction (Action<Vector3> action, Vector3 initialValue, Vector3 desiredValue, float animDuration)
		{
			return PerformFunction (action, initialValue, desiredValue, animDuration, E.Linear);
		}

		public static IEnumerator PerformFunction (Action<Vector3> action, Vector3 initialValue, Vector3 desiredValue, float animDuration, EasingFunction f)
		{
			float dX = desiredValue.x - initialValue.x;
			float dY = desiredValue.y - initialValue.y;
			float dZ = desiredValue.z - initialValue.z;   
			float sumTime = 0f;
			while (sumTime < animDuration) {
				action (
					new Vector3 (
						f (sumTime, initialValue.x, dX, animDuration),
						f (sumTime, initialValue.y, dY, animDuration),
						f (sumTime, initialValue.y, dZ, animDuration)
					)
				);
				sumTime += Time.deltaTime;
				yield return null;
			}
			//need to be sure
			action (desiredValue);   
		}

		#endregion

		#region Animations on objects

		public static IEnumerator ChangeColor (Action<Color> c, Color initialColor, Color desiredColor, float animDuration, TimeMode tm = TimeMode.NORMAL)
		{
			return ChangeColor (c, initialColor, desiredColor, animDuration, E.Linear, tm);
		}

		public static IEnumerator ChangeColor (Action<Color> c, Color initialColor, Color desiredColor, float animDuration, EasingFunction f, TimeMode tm = TimeMode.NORMAL)
		{
			float sumTime = 0f;
			while (sumTime < animDuration) {
				c (
					Lerp(initialColor, desiredColor, sumTime / animDuration)
				);
				EvaluateTime (ref sumTime, tm);
				yield return null;
			}
			//need to be sure
			c (desiredColor);
		}

		public static IEnumerator MoveTowards (Transform t, Vector3 desiredPos, float animDuration, TimeMode tm = TimeMode.NORMAL)
		{
			return MoveTowards (t, desiredPos, animDuration, E.Linear, tm);
		}

		public static IEnumerator MoveTowards (Transform t, Vector3 desiredPos, float animDuration, EasingFunction f, TimeMode tm = TimeMode.NORMAL)
		{
			Vector3 basePosition = t.localPosition;
			float sumTime = 0f;
			while (sumTime < animDuration) {
				t.localPosition = Lerp (basePosition, desiredPos, sumTime / animDuration, f);
				EvaluateTime (ref sumTime, tm);
				yield return null;
			}
			//I need to be sure
			t.localPosition = desiredPos;
		}

		// NOT_TESTED yet
		public static IEnumerator MoveBy(Transform t, Vector3 byVector, float animDuration, TimeMode tm = TimeMode.NORMAL) {
			return MoveBy (t, byVector, animDuration, E.Linear, tm);
		}

		// NOT_TESTED yet
		public static IEnumerator MoveBy(Transform t, Vector3 byVector, float animDuration, EasingFunction f, TimeMode tm = TimeMode.NORMAL) {
			Vector3 basePosition = t.localPosition;
			Vector3 desiredPosition = basePosition + byVector;

			float sumTime = 0f;
			while (sumTime < animDuration) {
				t.localPosition = Lerp (basePosition, desiredPosition, sumTime / animDuration, f);
				EvaluateTime (ref sumTime, tm);
				yield return null;
			}

			t.localPosition = desiredPosition;
		}

		public static IEnumerator RotateTowards(Transform t, Quaternion desiredRotation, float animDuration, TimeMode tm = TimeMode.NORMAL) {
			return RotateTowards (t, desiredRotation, animDuration, E.Linear, tm);
		}

		public static IEnumerator RotateTowards(Transform t, Quaternion desiredRotation, float animDuration, EasingFunction f, TimeMode tm = TimeMode.NORMAL)
		{
			float sumTime = 0f;
			Quaternion originalRotation = t.localRotation;
			while (sumTime < animDuration) {
				t.localRotation = Slerp (originalRotation, desiredRotation, sumTime / animDuration, f);
				EvaluateTime (ref sumTime, tm);
				yield return null;
			}
			t.localRotation = desiredRotation;
		}

		public static IEnumerator ScaleTo (Transform t, Vector3 desiredSize, float animDuration, TimeMode tm = TimeMode.NORMAL)
		{
			return ScaleTo (t, desiredSize, animDuration, E.Linear, tm);
		}

		public static IEnumerator ScaleTo (Transform t, Vector3 desiredSize, float animDuration, EasingFunction f,TimeMode tm = TimeMode.NORMAL)
		{
			Vector3 baseSize = t.localScale;
			float sumTime = 0f;
			while (sumTime < animDuration) {
				t.localScale = Lerp (baseSize, desiredSize, sumTime / animDuration);
				EvaluateTime (ref sumTime, tm);
				yield return null;
			}
			//I need to be sure
			t.localScale = desiredSize;
		}

		#endregion

		#region Special Animation Events

		/**
		 * Execution is stopped when contition == true
		 */
		public static IEnumerator Stoppable (Func<bool> condition, IEnumerator coroutine)
		{
			bool continued = true;
			while (!condition () && continued) {
				if (!coroutine.MoveNext ()) {
					continued = false;
				}
                
				yield return null;
			}
		}

		/**
		 * Execution is paused when contition == false
		 */
		public static IEnumerator Pauseable (Func<bool> condition, IEnumerator coroutine)
		{
			bool continued = true;
			while (continued) {
				if (!condition ()) {
					if (!coroutine.MoveNext ()) {
						continued = false;
					}
				} 
				yield return null;
			}
		}

		/**
		 * Pauses animation queue for specified seconds.
		 */
		public static IEnumerator Wait (float seconds, TimeMode tm = TimeMode.NORMAL)
		{
			float sumTime = 0;
			while (sumTime < seconds) {
				EvaluateTime (ref sumTime, tm);
				yield return null;
			}
		}

		public static IEnumerator WaitOrSpace(float seconds, TimeMode tm = TimeMode.NORMAL) {
			float sumTime = 0;
			while(sumTime < seconds) {
				EvaluateTime(ref sumTime, tm);
				if(Input.GetKeyDown(KeyCode.Space)) {
					yield break;
				}
				yield return null;
			}
		}

		/// <summary>
		/// Executes the void() method as IEnumerator
		/// (so it can be inserted into Stack(params[anim]
		/// </summary>
		/// <example>
		/// <code>
		/// this.StartCoroutine(
		///     A.Stack(
		///         A.MoveTowards(this.transform, Vector3.zero, 5f),
		///         A.Wait(3f),
		///         A.Void(() => 
		///             {
		///                 GameObject.Destroy(this.gameobject);
		///             }
		///         )
		///     )
		/// );
		/// </code>
		/// </example>
		/// <param name="action">Action.</param>
		public static IEnumerator Void (Action action)
		{
			action ();
			yield return null;
		}

		public static IEnumerator Void<V> (Action<V> action, V val)
		{
			action (val);
			yield return null;
		}

		public static IEnumerator Void<V0,V1> (Action<V0, V1> action, V0 val0, V1 val1)
		{
			action (val0, val1);
			yield return null;
		}

		#endregion

		#region MultiAnimations joining few animations together

		/// <summary>
		/// Chain the specified anims and perform them one-by-one.
		/// Next anim starts frame after previous finishes.
		/// Lasts as long as all the animations together.
		/// </summary>
		/// <param name="anims">IEnumerators to perform</param>
		public static IEnumerator Chain (params IEnumerator[] anims)
		{
			for (int i = 0; i < anims.Length; i++) {
				while (anims [i].MoveNext ()) {
					yield return anims [i].Current;
				}
			}
		}

		/// <summary>
		/// Executes all the anims at once.
		/// All anim starts immidietly.
		/// Lasts as long as the longest of IEnumerators it's performing.
		/// </summary>
		/// <param name="anims">IEnumerators to perform</param>
		public static IEnumerator Parallel (params IEnumerator[] anims)
		{
			bool any;
			do {
				any = false;
				for (int i = 0; i < anims.Length; i++) {
					if (anims [i].MoveNext ()) {
						any = true;
					}
				}
				yield return null;
			} while(any);
		}

		#endregion

		#region Helper methods
		static void EvaluateTime(ref float sumTime, TimeMode mode) {
			sumTime += (mode.Equals (TimeMode.NORMAL)) ? Time.deltaTime : Time.unscaledDeltaTime;
		}
		#endregion
	}
}  