﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SingleScoreCellView : MonoBehaviour {

	#region MEMBERS
	public Text userNameText;
	public Text userScore;
	#endregion


	#region METHODS
	public void Init(SingleScoreRecord record) {
		this.userNameText.text = record.playerName;
		this.userScore.text = record.points.ToString();
	}
	#endregion
}
