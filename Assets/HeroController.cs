﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroController : MonoBehaviour {

    float horizontalPosition = 0.0f;
    const float SPEED = 3.0f;
    const float WAVE_HEIGHT = 1.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.RightArrow))
        {
            horizontalPosition += SPEED * Time.deltaTime;
        }

        var pos = transform.localPosition;
        pos.x = horizontalPosition;
        pos.y = Mathf.Sin(horizontalPosition) * WAVE_HEIGHT;
        transform.localPosition = pos;
	}
}
