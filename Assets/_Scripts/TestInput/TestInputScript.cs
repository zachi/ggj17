﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestInputScript : MonoBehaviour {
	void Update() {
		this.transform.localPosition = new Vector2 (
			Input.GetAxis (UnityConstants.Axes.Horizontal), 
			Input.GetAxis (UnityConstants.Axes.Vertical)
		);
	}
}
