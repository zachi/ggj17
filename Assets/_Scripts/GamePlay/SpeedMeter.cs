﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedMeter : MonoBehaviour {

	public static SpeedMeter Instance {
		get; set;
	}

	void Awake() {
		Instance = this;
	}

	public void SetVelocity(float f) {
		this.transform.rotation = Quaternion.Euler(0f, 0f, 90f - f);
	}


}
