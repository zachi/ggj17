﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	public Transform spyOnTransform;

	void LateUpdate() {
		this.transform.position = this.spyOnTransform.position + new Vector3(
			spyOnTransform.position.y
			, 0f, -10f);
		Camera.main.orthographicSize = this.transform.position.y *.85f;
	}
}
