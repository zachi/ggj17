﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WavesController : MonoBehaviour {

	public GameObject[] prefabs;
	public GameObject[] skies;
	float nextWaveAt = 0;
	public float betweenWaves = 25f;

	void Start() {
		for(int i = 0; i < 100; i++) {
			GameObject next = GameObject.Instantiate(this.prefabs[Random.Range(0, this.prefabs.Length - 1)], this.transform);
			next.transform.localPosition = new Vector3(nextWaveAt, 0f, 0f);

			nextWaveAt += betweenWaves;

		}

		nextWaveAt = 0f;

		for(int i = 0; i < 200; i++) {
			
			GameObject next = GameObject.Instantiate(this.skies[Random.Range(0, this.skies.Length - 1)], this.transform);
			next.transform.localPosition = new Vector3(nextWaveAt, 10f, 0f);
			nextWaveAt += (betweenWaves * .5f);

		}
	}

}
