﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class GameTimer : MonoBehaviour {

	[SerializeField] readonly float TimeTillEnd = 2*60;

	public event Action TimerFinished;

	public string LastKnownText {
		get;
		set;
	}

	public bool Running {
		get; set;
	}

	public static GameTimer Instance {
		get; private set;
	}

	float CurrTimeTillEnd {
		get; set;
	}

	Text CachedText {
		get;
		set;
	}

	void Awake() {
		Instance = this;
		CachedText = this.GetComponent<Text>();
	}

	public void StartGame() {
		this.CurrTimeTillEnd = TimeTillEnd;
		Points.Instance.ResetPoints();
		this.Running = true;
	}

	void Update() {
		if(Running) {
			if(CurrTimeTillEnd > 0) {
				CurrTimeTillEnd -= Time.deltaTime;
			}
			if(CurrTimeTillEnd <= 0) {
				CurrTimeTillEnd = 0;
				Running = false;
				if(TimerFinished != null) {
					TimerFinished.Invoke();
				}
			}
		}
		string ttf = Format((int)CurrTimeTillEnd);
		if(ttf != LastKnownText) {
			this.LastKnownText = ttf;
			this.CachedText.text = ttf;
		}
	}

	public static string Format(int seconds)
	{
		System.TimeSpan t = new System.TimeSpan(
			0, 0, seconds
		);

		return string.Format("{0:#0}:{1:00}", t.Minutes, t.Seconds);
	}

}
