﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class MinimalRight : MonoBehaviour {

	public float speedUpFactor = .01f;
	public float jumpFactor = 200f;
	public float OnWaveDistanceCheck = .1f;
	public float HugeMass = 3f;
	public float windRight = 20f;

	public float minimalJumpForBonus = 5f;

	public SpriteRenderer rend;
	public Sprite spriteDown;
	public Sprite spriteNormal;
	public Sprite spriteUp;

	public Transform viewTransform;

	Rigidbody2D body {
		get;
		set;
	}

	bool OnWave {
		get {
			return 
				Physics2D.Raycast(this.transform.position, Vector2.down + Vector2.right, OnWaveDistanceCheck, UnityConstants.Layers.WavesMask)
				|| Physics2D.Raycast(transform.position, Vector2.down, OnWaveDistanceCheck, UnityConstants.Layers.WavesMask)
				|| Physics2D.Raycast(transform.position, Vector2.down + Vector2.left, OnWaveDistanceCheck, UnityConstants.Layers.WavesMask);
		}
	}

	bool _prevFrameOnWave;

	bool PreviousFrameOnWave {
		get { return _prevFrameOnWave; }
		set {
			if(value && !_prevFrameOnWave) {
				float currX = this.transform.position.x;
				if(currX - jumpOffX > minimalJumpForBonus) {
					Points.Instance.AddPoints(Mathf.CeilToInt(currX - jumpOffX));
				}
			} else if ( _prevFrameOnWave && !value) {
				this.jumpOffX = this.transform.position.x;
			}
			this._prevFrameOnWave = value;
		}
	}

	float jumpOffX {
		get; set;
	}

	float OriginalMass {
		get; set;
	}

	void Awake() {
		this.body = this.GetComponent<Rigidbody2D>();
		this.OriginalMass = this.body.mass;
	}

	void LateUpdate() {
		this.viewTransform.rotation = Quaternion.Euler(Vector3.zero);
	}

	void FixedUpdate() {
		if(!this.OnWave) {
			this.body.AddForce(Vector2.right * windRight * Time.fixedDeltaTime);
			if(Input.GetKey(KeyCode.RightArrow) && body.velocity.x < 25f) {
				this.body.AddForce(Vector2.right * windRight * Time.deltaTime * 20f);
			}
			if(Input.GetKey(KeyCode.LeftArrow)) {
				this.body.AddForce(Vector2.left * windRight * Time.deltaTime * 7f);
			}
			if(Input.GetKey(KeyCode.DownArrow)) {
				this.body.AddForce(Vector2.down * HugeMass * Time.fixedDeltaTime);
			}
		} else if(this.OnWave && body.velocity.magnitude < 1f && Input.GetKeyDown(KeyCode.R)) {
			this.transform.position = this.transform.position + Vector3.up * 4f;
			this.body.velocity = Vector3.right;
		}
	}

	void Update() {
		if(OnWave && !this.GetComponent<ParticleSystem>().isPlaying) {
			this.GetComponent<ParticleSystem>().Play();
		}

		if(!OnWave) {
			this.GetComponent<ParticleSystem>().Stop();
		}
		if(this.body.velocity.y > .5f) {
			this.rend.sprite = this.spriteUp;
		} else if (this.body.velocity.y < -.5f) {
			this.rend.sprite = this.spriteDown;
		} else {
			this.rend.sprite = this.spriteNormal;
		}

		SpeedMeter.Instance.SetVelocity(this.body.velocity.magnitude * 3f);
	}

	void OnTriggerStay2D(Collider2D collider) {
		if(collider.gameObject.layer == UnityConstants.Layers.TDown && this.OnWave) {
			if(Input.GetKeyDown(KeyCode.Space)) {
				this.body.AddForce(Vector2.up * jumpFactor * this.body.velocity.magnitude * Time.fixedDeltaTime);
			} else {
				this.body.mass = .2f;
				this.body.AddForce((Vector2.up + Vector2.right) * Time.fixedDeltaTime * speedUpFactor);
			}
		} else if ( collider.gameObject.layer == UnityConstants.Layers.TUp && this.OnWave && !this.PreviousFrameOnWave ) {
			this.body.AddForce(Vector2.down * Time.fixedDeltaTime * speedUpFactor);
		} else {
			this.body.mass = OriginalMass;
		}
		PreviousFrameOnWave = this.OnWave;
	}

}
