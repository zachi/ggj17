﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndGame : MonoBehaviour {

	public GameObject EndGameImage;
	public Text YourScoreText;
	const string YourScoreTextPattern = "Your\nScore:\n{0}";
	public InputField refInput;
	public int pointsToWin = 5000;

	public Image WinLooseSpriteRenderer;
	public Sprite winBg;
	public Sprite looseBg;

	public Image WinLooseAnimation;
	public Sprite[] winAnim;
	public Sprite[] looseAnim;


	// Use this for initialization
	void Start () {
		GameTimer.Instance.TimerFinished += this.OnGameEnds;
	}

	void OnGameEnds() {
		EndGameImage.SetActive(true);
		this.YourScoreText.text = string.Format(YourScoreTextPattern, Points.Instance.Score);
		this.refInput.ActivateInputField();
		refInput.text = string.Empty;

		if(Points.Instance.Score >= this.pointsToWin) {
			WinLooseSpriteRenderer.sprite = winBg;
			this.StartCoroutine(ShowAnimation(winAnim));
		} else {
			WinLooseSpriteRenderer.sprite = looseBg;
			this.StartCoroutine(ShowAnimation(looseAnim));
		}
	}

	IEnumerator ShowAnimation(Sprite[] s) {
		int i =0;
		while(true) {
			i++;
			if(i == s.Length)
				i = 0;
			this.WinLooseAnimation.sprite = s[i];
			yield return new WaitForSeconds(.4f);
		}
	}

	public void SubmitScore() {
		string playerName = refInput.text;
		int score = Points.Instance.Score;
		ScoresController.Instance.AddScore(
			new SingleScoreRecord( playerName, score)
		);

		this.StopAllCoroutines();
		EndGameImage.SetActive(false);
	}
}
