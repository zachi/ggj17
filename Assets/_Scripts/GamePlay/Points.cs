﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class Points : MonoBehaviour {

	public InputField inputFieldReference;

	int points = 0;

	public int Score {
		get { return points; }
	}

	Text CachedText {
		get; set;
	}

	public static Points Instance {
		get; set;
	}

	void Awake() {
		CachedText = this.GetComponent<Text>();
		Instance = this;
	}

	public void ResetPoints() {
		this.points = 0;
		this.CachedText.text = points.ToString();
	}

	public void AddPoints(int p) {
		this.points += p;
		this.CachedText.text = points.ToString();
	}



}