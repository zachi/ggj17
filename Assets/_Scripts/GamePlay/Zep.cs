﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AnimPlugin;

public class Zep : MonoBehaviour {

	public Transform player;
	public GameObject ExplosionPrefab;

	public Sprite explosionSprite;

	float originalY;

	bool IgnoreCol = false;

	SpriteRenderer CachedSpriteRenderer {
		get; set;
	}

	public Vector3 originalScale {
		get; set;
	}

	Sprite originalSprite {
		get; set;
	}

	void Awake() {
		this.CachedSpriteRenderer = this.GetComponent<SpriteRenderer>();
		originalY = this.transform.position.y;
		originalSprite = this.CachedSpriteRenderer.sprite;
	}

	void LateUpdate () {
		this.transform.position = Vector3.Lerp(this.transform.position, new Vector3(player.position.x, originalY, this.transform.position.z), .5f);
	}

	void OnCollisionEnter2D(Collision2D collision) {
		if(IgnoreCol) 
			return;

		IgnoreCol = true;
		this.StartCoroutine(
			A.Chain(
				A.Wait(.75f),
				A.Void( () => { this.IgnoreCol = false; } )
			)
		);

		GameObject g = GameObject.Instantiate(this.ExplosionPrefab, this.transform);

		g.transform.position = (Vector3)collision.contacts[0].point + Vector3.up * 2f;
		g.GetComponent<ParticleSystem>().Play();
		Points.Instance.AddPoints(1000);

		this.StartCoroutine(
			A.Chain( 
				A.Void(
					() => { CachedSpriteRenderer.sprite = explosionSprite; 
					}),
				A.PerformFunction(F, this.originalY, this.originalY + 1f, 2.5f, E.BackOut),
				A.Void( () => { CachedSpriteRenderer.sprite = originalSprite; } )

			)
		);
	}

	public void F(float f) {
		this.originalY = f;
	}
}
