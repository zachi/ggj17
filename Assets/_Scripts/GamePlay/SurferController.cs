﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurferController : MonoBehaviour {

	public LayerMask WavesPhysicsMask;
	public Transform BottomCheck;
	public Vector2 Velocity;
	public float SpeedFactor = 1f;

	public float FactorSpeedUp = 1.1f;
	public float FactorHoldDown = .9f;
	public float FactorHoldBig = .75f;

	public Transform previousPoint;
	public Transform nextPoint;

	public enum WavePhase {
		DOWN,
		UP,
		NEUTRAL
	}

	public enum SurferState {
		WAVE,
		AIR
	}

	public SurferState CurrSurferState {
		get;
		set;
	}

	public WavePhase CurrWavePhase {
		get;
		set;
	}

	void Update() {
		SurferState newSurferState = CurrSurferState;

		RaycastHit2D hit = Physics2D.Raycast(
			this.transform.position, 
			Vector2.down,
			Vector2.Distance(this.transform.position, BottomCheck.transform.position),
			WavesPhysicsMask
		);

		if(hit.collider != null && hit.collider.gameObject != null) {
			newSurferState = SurferState.WAVE;
		} else {
			newSurferState = SurferState.AIR;
		}

		if(CurrSurferState == SurferState.AIR && newSurferState == SurferState.WAVE) {
			switch(this.CurrWavePhase) {
				case WavePhase.DOWN:
					this.SpeedFactor *= FactorSpeedUp;
					break;
				case WavePhase.UP:
					this.SpeedFactor *= FactorHoldBig;
					break;
				case WavePhase.NEUTRAL:
					this.SpeedFactor *= FactorHoldDown;
					break;
			}
		} else if(CurrSurferState == SurferState.WAVE) {
			// surfer is on wave, gain or loose speed according to key / phase

			// move surfer to next point?
			this.transform.position = Vector3.Lerp(previousPoint.transform.position, nextPoint.transform.position, (nextPoint.transform.position.y - previousPoint.transform.position.y));

		} else {
			// surfer is in the air.
		}
	}

	void OnTriggerStay2D(Collider2D other) {
		if(other.gameObject.layer.Equals(UnityConstants.Layers.TDown)) {
			this.CurrWavePhase = WavePhase.DOWN;
		} else if (other.gameObject.layer.Equals(UnityConstants.Layers.TUp)) {
			this.CurrWavePhase = WavePhase.UP;
		} else {
			this.CurrWavePhase = WavePhase.NEUTRAL;
		}
	}
}
