﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SingleScoreRecord
{
	public string playerName;
	public int points;

	public SingleScoreRecord(string pName, int pScore) {
		this.playerName = pName;
		this.points = pScore;
	}
}
