﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AnimPlugin;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

	#region MEMBERS
	// add buttons here
	public RectTransform[] elements;
	public RectTransform mainPanel;
	public Camera c;
	bool ignoreSpace = false;

	public GameObject EndGameScreenReference;

	public GameObject[] Intro;
	int introCount = 0;

	public Text[] opts;

	int choosenOpt = 0;

	#endregion

	#region PROPERTIES
	Vector2 originalPanelPosition {
		get;
		set;
	}

	bool ScoreTableIsShown {
		get { return EndGameScreenReference.gameObject.activeInHierarchy; }
	}
	#endregion

	#region METHODS

	public void OnPlayButtonClicked() {
		Debug.Log("PLAY");

		this.StartCoroutine(
			A.Chain(
				RemoveMenu(),
				A.Void( () =>  { 
					SceneManager.LoadScene("WAVES_SCENE", LoadSceneMode.Additive); 
					GameTimer.Instance.StartGame();
				} )

			)
		);
	}

	public void OnBestResultsButtonClicked() {
		this.StartCoroutine(
			A.Chain(
				RemoveMenu(),
				A.Void( () =>  { SceneManager.LoadScene("RESULTS_SCENE"); } )
			)
		);
	}

	public void OnQuitButtonClicked() {
		Application.Quit();
	}

	public void OnReturn() {
		GameTimer.Instance.Running = false;
		this.StartCoroutine(
			A.Chain(
				A.Void( () => {
					try {
						SceneManager.UnloadSceneAsync("WAVES_SCENE"); 
					} catch(Exception e) {}
					try {
						SceneManager.UnloadSceneAsync("RESULTS_SCENE");
					} catch(Exception e) {}

				} ),
				ReturnMenu()
			)
		);
				
	}

	IEnumerator ReturnMenu() {
		yield return this.StartCoroutine(
			A.Chain(
				A.Void ( () => { c.gameObject.SetActive(true); } ),
			A.MoveTowards(mainPanel, originalPanelPosition, .7f, E.BackOut),
			A.Void( () => { for(int i = 0; i < this.elements.Length; i++) {
					this.elements[i].gameObject.SetActive(true);
				}
			} )
			)
		);
	}

	void Awake() {
		this.originalPanelPosition = this.mainPanel.transform.localPosition;
	}

	IEnumerator RemoveMenu() {
		ignoreSpace = true;
		yield return this.StartCoroutine(
			A.Chain(
				
				A.MoveBy(mainPanel, Vector3.up * Screen.height * 10, .7f, E.BackIn),

				A.Void(() => {this.Intro[0].SetActive(true); } ),
				A.WaitOrSpace(2f),
				A.Void(() => {this.Intro[1].SetActive(true); } ),
				A.WaitOrSpace(2f),
				A.Void(() => {this.Intro[2].SetActive(true); } ),
				A.WaitOrSpace(2f),
				A.Void(() => {this.Intro[3].SetActive(true); } ),
				A.WaitOrSpace(2f),
				A.Void(() => {this.Intro[4].SetActive(true); } ),
				A.WaitOrSpace(2f),
				A.Void(() => {this.Intro[5].SetActive(true); } ),
				A.WaitOrSpace(2f),
				A.Void(() => {this.Intro[6].SetActive(true); } ),
				A.WaitOrSpace(2f),
				A.Void(() => {this.Intro[7].SetActive(true); } ),
				A.WaitOrSpace(2f),

				A.Void( () => { for(int i = 0; i < this.elements.Length; i++) {
						this.elements[i].gameObject.SetActive(false);
					}
					for(int i = 0; i < this.Intro.Length; i++) {
						this.Intro[i].SetActive(false);
					}
				} ),
				A.Void( () => {
					c.gameObject.SetActive(false); 
					ignoreSpace = false;
				})
			)
		);
	}

	IEnumerator Start() {
		GameTimer.Instance.TimerFinished += this.OnReturn;

		while(true) {
			for(int i = 0; i < this.elements.Length; i++) {
				this.elements[i].localRotation = Quaternion.Euler(new Vector3(0f, 0f, Mathf.Sin(Time.time + i) * (5f + i)));
			}
			if(choosenOpt == 0) {
				this.opts[0].color = Color.black;
				this.opts[1].color = new Color(1f, Mathf.Sin(Time.time), Mathf.Sin(Time.time), 1f);
			} else {
				this.opts[1].color = Color.black;
				this.opts[0].color = new Color(1f, Mathf.Sin(Time.time), Mathf.Sin(Time.time), 1f);
			}
			yield return false;
		}
	}


	// TODO - remove from final build
	void Update() {
		if(Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.DownArrow)) {
			this.choosenOpt +=1;
			if(this.choosenOpt == 2) {
				this.choosenOpt = 0;
			} else if(choosenOpt < 0) {
				this.choosenOpt = 1;
			}
		}
		if(!GameTimer.Instance.Running && !ScoreTableIsShown && !ignoreSpace) {
			if(Input.GetKeyDown(KeyCode.Space)) {
				if(choosenOpt == 0) {
					this.OnPlayButtonClicked();
				} else {
					this.OnQuitButtonClicked();
				}
			}
		}

		if(Input.GetKeyDown(KeyCode.Backspace)) {
			this.OnReturn();
		}
	}

	#endregion
}
