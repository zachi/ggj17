﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoresController : MonoBehaviour, IComparer<SingleScoreRecord> {

	#region EVENTS
	public static event Action<ScoresController> ScoreChangeFinishedEv;
	#endregion

	#region MEMBERS
	const string PREF_NAME = "PPREFS_SCORES";
	const int POSITIONS_TO_REMEMBER = 6;
	public List<SingleScoreRecord> records;

	#endregion

	#region PROPERTIES
	public static ScoresController Instance {
		get;
		private set;
	}
	#endregion

	#region METHODS

	void Awake() {
		Instance = this;
	}

	void Start() {
		this.records = new List<SingleScoreRecord>();
		string loadedString = PlayerPrefs.GetString(PREF_NAME, string.Empty);

		if(!string.IsNullOrEmpty(loadedString)) {
			JsonUtility.FromJsonOverwrite(loadedString, this);
		}

		if(ScoreChangeFinishedEv != null) {
			ScoreChangeFinishedEv.Invoke(this);
		} else {
			Debug.Log("asdasd");
		}
	}

	public void AddScore(SingleScoreRecord record) {
		this.records.Add(record);
		this.records.Sort(this);
		while(records.Count > POSITIONS_TO_REMEMBER) {
			records.Remove(records[records.Count - 1]);
		}

		PlayerPrefs.SetString(PREF_NAME, JsonUtility.ToJson(this));

		if(ScoreChangeFinishedEv != null) 
			ScoreChangeFinishedEv.Invoke(this);
	}

	#region IComparer implementation

	public int Compare(SingleScoreRecord x, SingleScoreRecord y)
	{
		return y.points - x.points;
	}

	#endregion
	#endregion

}
