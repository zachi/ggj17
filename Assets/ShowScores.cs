﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowScores : MonoBehaviour {

	List<GameObject> toDestroy;
	public GameObject singleRecordPrefab;

	void Awake() {
		toDestroy = new List<GameObject>();
		ScoresController.ScoreChangeFinishedEv += ScoresController_ScoreChangeFinishedEv;
	}

	void ScoresController_ScoreChangeFinishedEv (ScoresController obj)
	{
		while(toDestroy.Count > 0) {
			GameObject t = toDestroy[0];
			toDestroy.Remove(toDestroy[0]);
			GameObject.Destroy(t);
		}

		for(int i = 0; i< ScoresController.Instance.records.Count; i++) {
			GameObject g = GameObject.Instantiate(singleRecordPrefab, this.transform);
			g.GetComponent<SingleScoreCellView>().Init(ScoresController.Instance.records[i]);
			g.transform.localScale = Vector3.one;
			this.toDestroy.Add(g);
		}
	}

}
